# My neighbors V0.1   <img src="https://media.giphy.com/media/KZHOFKIzfTLD4LJVkF/giphy.gif" width="100px"> <br /> 

<img src="https://academicpositions.it/uploads/068/2d0/0682d095e8b1cb2a28380436cc75f4c0.png" width="400px" align="right">

### Hey  <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px"> <br /> 
<br />

## Participant :
| *Nom et Prenom* | *Pseudo* |
| ------ | ------ |
| M'hamed Lotfi | ElonLotfi |



---


Bonjour, <br /> 

Collaborateur du projet :<br>


🙍🏽‍♂️ [@Lotfi](https://github.com/ElonLotfi) <br>



encadrant 👨🏽‍💼[@M.Amosse] <br /> 



<br /> 
<br /> 

## Objectif de l'application :

Créez une application permettant de mise en relation entre voisins . <br>

## Fonctionnalités implémentées  <img src="https://media.giphy.com/media/26vwfMVM6nlEkwftUj/giphy.gif" width="40px"> <br /> 
✅ Afficher la liste des voisins <br>
✅ Ajouter un voisin <br>
✅ champs de validation du formulaire qui permet d'ajouter un voisin <br>
✅ Supprimer un voisin<br>
✅ Internationalisation de l'application (anglais + francais) <br>


## Une application Web develppé à l'aide de:<br>
 `Android & Kotlin`<br>







