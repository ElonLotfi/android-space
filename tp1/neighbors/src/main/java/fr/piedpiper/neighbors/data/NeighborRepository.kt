package fr.piedpiper.neighbors.data

import fr.piedpiper.neighbors.data.service.DummyNeighborApiService
import fr.piedpiper.neighbors.data.service.NeighborApiService
import fr.piedpiper.neighbors.models.Neighbor

class NeighborRepository {
    private val apiService: NeighborApiService

    init {
        apiService = DummyNeighborApiService()
    }

    fun getNeighbours(): List<Neighbor> = apiService.neighbours

    fun delNeighbours(neighbor: Neighbor) = apiService.deleteNeighbour(neighbor)

    fun addNeighbour(neighbor: Neighbor) = apiService.createNeighbour(neighbor)

    companion object {
        private var instance: NeighborRepository? = null
        fun getInstance(): NeighborRepository {
            if (instance == null) {
                instance = NeighborRepository()
            }
            return instance!!
        }
    }
}