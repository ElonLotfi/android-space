package fr.piedpiper.neighbors.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import fr.piedpiper.neighbors.NavigationListener
import fr.piedpiper.neighbors.R
import fr.piedpiper.neighbors.adapters.ListNeighborHandler
import fr.piedpiper.neighbors.adapters.ListNeighborsAdapter
import fr.piedpiper.neighbors.data.NeighborRepository
import fr.piedpiper.neighbors.models.Neighbor
import androidx.appcompat.app.AppCompatActivity




class ListNeighborsFragment : Fragment() , ListNeighborHandler {

    private lateinit var recyclerView: RecyclerView
    private lateinit var addNeighbor: FloatingActionButton






    /**
     * Fonction permettant de définir une vue à attacher à un fragment
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.list_neighbors_fragment, container, false)
        recyclerView = view.findViewById(R.id.neighbors_list)
        addNeighbor = view.findViewById(R.id.btnAddNeighbour)
        recyclerView.layoutManager = LinearLayoutManager(context)

        // Change toolbar Title
        (activity as? NavigationListener)?.let {
            it.updateTitle("List of neighbors")
        }

        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )
        addNeighbor.setOnClickListener {
            (activity as? NavigationListener)?.let {
                it.showFragment(AddNeighbourFragment())
            }
        }
        return view
    }

    private fun displayData() {
        val neighbors = NeighborRepository.getInstance().getNeighbours()
        val adapter = ListNeighborsAdapter(neighbors, this)
        recyclerView.adapter = adapter
    }


    // Surcharges la fonction onViewCreated du fragment pour ajouter l'adapter au recyclerview
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        displayData()


    }

    override fun onDeleteNeibor(neighbor: Neighbor) {
        NeighborRepository.getInstance().delNeighbours(neighbor)
        displayData()

    }




}