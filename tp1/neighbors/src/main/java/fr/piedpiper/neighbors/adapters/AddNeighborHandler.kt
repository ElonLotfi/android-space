package fr.piedpiper.neighbors.adapters

import fr.piedpiper.neighbors.models.Neighbor

interface AddNeighborHandler {
    fun addNeighbor(neighbor: Neighbor)

}