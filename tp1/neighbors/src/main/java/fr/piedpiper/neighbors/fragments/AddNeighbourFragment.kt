package fr.piedpiper.neighbors.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputLayout
import fr.piedpiper.neighbors.NavigationListener
import fr.piedpiper.neighbors.R
import fr.piedpiper.neighbors.adapters.AddNeighborHandler
import fr.piedpiper.neighbors.data.NeighborRepository
import fr.piedpiper.neighbors.models.Neighbor
import java.util.regex.Pattern
import android.view.animation.TranslateAnimation
import android.widget.Toast


class AddNeighbourFragment : Fragment() , AddNeighborHandler  {


    private lateinit var backToNeighboursList: FloatingActionButton
    private lateinit var neighborName: TextInputLayout
    private lateinit var neighborImage: TextInputLayout
    private lateinit var neighborPhone: TextInputLayout
    private lateinit var neighborWebSite: TextInputLayout
    private lateinit var neighborAdresse : TextInputLayout
    private lateinit var neighborInformation : TextInputLayout
    private lateinit var buttonSaveNeighbor : Button
    private lateinit var avatar : ImageView

    /**
     * Function to define a view to attach to a fragment
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.add_neighbor, container, false)
        backToNeighboursList = view.findViewById(R.id.btn_back)
        backToNeighboursList.setOnClickListener {
            (activity as? NavigationListener)?.showFragment(ListNeighborsFragment())
        }
        (activity as? NavigationListener)?.let {
            it.updateTitle("Add a neighbor")
        }

        neighborName = view.findViewById(R.id.neighbor_name)
        neighborImage = view.findViewById(R.id.neighbor_image)
        neighborPhone = view.findViewById(R.id.neighbor_phone)
        neighborWebSite = view.findViewById(R.id.neighbor_website)
        neighborAdresse = view.findViewById(R.id.adresse)
        neighborInformation = view.findViewById(R.id.neighbor_informations)
        buttonSaveNeighbor = view.findViewById(R.id.save_neighbour)
        avatar = view.findViewById(R.id.avatar)
        setup()
        buttonSaveNeighbor.setOnClickListener {
            val newNeighbour =
                Neighbor(getNeiNeighborsNumbers().toLong() + 1
                    , neighborName.editText?.text.toString()
                    , neighborImage.editText?.text.toString()
                    , neighborAdresse.editText?.text.toString()
                    , neighborPhone.editText?.text.toString()
                    , neighborInformation.editText?.text.toString()
                    , false,
                    neighborWebSite.editText?.text.toString())
            addNeighbor(newNeighbour)
            // Back to neighbours list
            displayToast(neighborName.editText?.text.toString())
            (activity as? NavigationListener)?.showFragment(ListNeighborsFragment())
        }
        return view
    }

    // Function to add a neighbor
    override fun addNeighbor(neighbor: Neighbor) {
        NeighborRepository.getInstance().addNeighbour(neighbor)
    }

    // Recover Neighbours list size
    private fun getNeiNeighborsNumbers(): Int {
        return NeighborRepository.getInstance().getNeighbours().size
    }
    // Allows you to validate the data in the form fields
    private fun setup(){
        buttonSaveNeighbor.isEnabled = false
        this.checkFields(neighborName)
        this.checkFields(neighborAdresse)
        this.checkFields(neighborInformation)
        this.checkUrl(neighborImage)
        this.checkUrl(neighborWebSite)
        this.checkPhone()
    }
    // Allows to activate the button
    private fun enableSaveNeighbour(){
        buttonSaveNeighbor.isEnabled =
            neighborName.editText?.text!!.isNotBlank() && neighborImage.editText?.text!!.isNotBlank()
                    && neighborPhone.editText?.text!!.isNotBlank() && neighborWebSite.editText?.text!!.isNotBlank()
                    && neighborAdresse.editText?.text!!.isNotBlank() && neighborInformation.editText?.text!!.isNotBlank()
    }

    private fun checkFields(textInputLayout: TextInputLayout) {
        textInputLayout.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isEmpty() == true) {
                    textInputLayout.error = "required fields"
                    textInputLayout.isErrorEnabled = true
                    buttonSaveNeighbor.isEnabled = false
                } else if (p0?.length!! > 30 && textInputLayout.hint!!.equals("about me")) {
                    textInputLayout.error = "The field can have a maximum of 30 characters"
                    textInputLayout.isErrorEnabled = true
                    buttonSaveNeighbor.isEnabled = false
                } else {
                    textInputLayout.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(p0: Editable?) {
                enableSaveNeighbour()

            }
        })
    }
    private fun checkPhone() {
        neighborPhone.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isEmpty() == true) {
                    neighborPhone.error = "Phone number is required"
                    neighborPhone.isErrorEnabled = true
                    buttonSaveNeighbor.isEnabled = false
                }
                else if (!Pattern.matches("^(0)(6|7)\\d{8}\$", p0)) {
                    neighborPhone.error = "format must be 0X XX XX XX XX"
                    neighborPhone.isErrorEnabled = true
                    buttonSaveNeighbor.isEnabled = false
                }

                else {
                    neighborPhone.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(p0: Editable?) {
                enableSaveNeighbour()
            }
        })
    }

    private fun checkUrl(textInputLayout: TextInputLayout) {
        textInputLayout.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isEmpty() == true) {
                    textInputLayout.error = "WebSite is required"
                    textInputLayout.isErrorEnabled = true
                    buttonSaveNeighbor.isEnabled = false
                } else if (!Patterns.WEB_URL.matcher(p0).matches()) {
                    textInputLayout.error = "WebSite URL dismatch"
                    textInputLayout.isErrorEnabled = true
                    buttonSaveNeighbor.isEnabled = false

                } else {
                    textInputLayout.isErrorEnabled = false
                }
            }
            override fun afterTextChanged(p0: Editable?) {
                enableSaveNeighbour()
                if(textInputLayout.hint!!.equals("image")){
                    val context = avatar.context
                    Glide.with(context)
                        .load(p0.toString())
                        .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.ic_baseline_person_outline_24)
                        .error(R.drawable.ic_baseline_person_outline_24)
                        .skipMemoryCache(false)
                        .into(avatar)
                    movePicture()
                }
            }
        })
    }


    // Faire bouger l'image lorsque son adresse est mis
    fun movePicture(){
        val animation = TranslateAnimation(0.0f, 50.0f, 0.0f, 0.0f)
        animation.duration = 700
        animation.repeatCount = 5
        animation.repeatMode = 2
        animation.fillAfter = true
        avatar.startAnimation(animation)
    }

    //Display a message when the neighbor has been successfully added
    private fun displayToast(name :String){
        val toast = Toast.makeText(context, "$name has been successfully added", Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }


}