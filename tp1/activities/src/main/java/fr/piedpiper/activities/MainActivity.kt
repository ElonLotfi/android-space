package fr.piedpiper.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView


class MainActivity : AppCompatActivity() {
    private lateinit var clickButton: Button
    private lateinit var btnCompute: Button
    private lateinit var textView: TextView
    private var nbClick = 0



    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setContentView(R.layout.activity_main)
        clickButton = findViewById(R.id.btn_click_me)
        textView = findViewById<TextView>(R.id.textView) as TextView
        btnCompute = findViewById(R.id.btn_compute)


        clickButton.setOnClickListener {
            nbClick++
            val newText = "Cliquez moi $nbClick"
            if(nbClick != 0){
                textView.text = newText;
            }
            if(nbClick == 5){
                clickButton.isEnabled = false;
            }

            clickButton.text = newText
        }

        btnCompute.setOnClickListener {
            val intent = Intent(baseContext, ComputeActiviy::class.java)
            startActivity(intent)
        }





    }
}