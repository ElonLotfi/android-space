package fr.piedpiper.activities

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import fr.piedpiper.activities.databinding.ComputeActivityBinding


class ComputeActiviy : AppCompatActivity(), TextWatcher {

    private lateinit var binding: ComputeActivityBinding


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        //setContentView(R.layout.compute_activity)
        binding = ComputeActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUp()
    }

    private fun setUp() {
        // disable the button
        binding.btnCalculer.isEnabled = false
        //first field
        binding.field1.addTextChangedListener(this)
        // second field
        binding.field2.addTextChangedListener(this)

        binding.btnCalculer.setOnClickListener({
            val result = makeAddition()
            // faut eviter le null si non ca va declencher un erreur
            binding.resultat.text = result.toString()
        })
    }

    fun makeAddition(): Int {
        var result = 0
        val firstValue = binding.field1.getText().toString().toInt()
        val secondValue = binding.field2.getText().toString().toInt()
        result = firstValue + secondValue
        return result
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun afterTextChanged(p0: Editable?) {
        binding.btnCalculer.isEnabled =
            binding.field1.text.isNotBlank() && binding.field2.text.isNotBlank()
    }
}